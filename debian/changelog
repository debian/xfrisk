xfrisk (1.2-9) unstable; urgency=medium

  * QA upload.
  * Fix building with -Werror=implicit-function-declaration.
    (Closes: #1066617)

 -- Andreas Beckmann <anbe@debian.org>  Thu, 04 Apr 2024 18:36:26 +0200

xfrisk (1.2-8) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 24 Aug 2021 21:04:03 -0300

xfrisk (1.2-7) experimental; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Bumped Standards-Version to 4.5.1.
      - Migrated Vcs-* fields to Salsa.
      - Minor changes in description.
  * debian/copyright:
      - Added a comment about a not valid non-free license.
      - Updated packaging copyright years and names.
  * debian/doc-base.xfrisk: renamed to doc-base.
  * debian/patches/:
      - Added a numeric prefix for all patches.
      - Added Last-Update field to all patches.
  * debian/README.source: created to explain about the branch
    public-upstream-versions in Salsa, that helps to explain about a not valid
    non-free license found in Aide.risk file in upstream place.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: created to provide a very basic CI test.
  * debian/upstream/metadata: created.
  * debian/watch: bumped version to 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 12 Jul 2021 10:53:48 -0300

xfrisk (1.2-6) unstable; urgency=medium

  * QA upload.
  * Rename patches_as_of_1.2-3.patch to xgethostname.patch and update
    DEP-3 patch description to actually describe what's changed instead of
    where the changes came from.
  * Ignore release-candidates in debian/watch. (The current
    release-candidate crashes and is not usable.)
  * Declare compliance with Debian Policy 4.1.1. (No changes needed.)
  * Set "Rules-Requires-Root: no".
  * Remove trailing whitespace from ancient debian/changelog entry and fix
    a typo in the same line.

 -- Axel Beckert <abe@debian.org>  Wed, 15 Nov 2017 05:47:36 +0100

xfrisk (1.2-5) unstable; urgency=high

  * QA upload.

  [ Trek ]
  * Recommend xfonts-75dpi or -100dpi since they are required by the X
    client, but not for the server alone. (Closes: #528058)

  [ Axel Beckert ]
  * Declare compliance with Debian Policy 4.1.0. (No changes needed.)
  * Update Homepage, debian/watch and debian/copyright to follow redirect
    from tuxick.net to point to www.tuxick.net and switch to HTTPS.
  * Disable any optimization for now, i.e. enforce usage of -O0. Even -O1
    causes xfrisk to reproducibly crash.  Thanks to Adrian Bunk for
    helping to debug this! (Closes: #874133)

 -- Axel Beckert <abe@debian.org>  Mon, 04 Sep 2017 02:20:17 +0200

xfrisk (1.2-4) unstable; urgency=low

  * QA upload
  * Set Maintainer to Debian QA Group. (See #869300)
  * Import packaging history into a collab-maint git repository with "gbp
    import-dscs --debsnap xfrisk" and add according Vcs-* headers.
  * Remove obsolete Emacs "local variables" from debian/changelog.
  * Apply wrap-and-sort.
  * Convert to source format "3.0 (quilt)".
  * Rewrite debian/rules as "%:\n\tdh $@".
    + Switch to compat level 10, update debhelper b-d accordingly.
    + Use debian/install instead of $(INSTALL_SCRIPT).
  * Declare compliance with Debian Policy 4.0.0.
  * Fix lintian warning copyright-refers-to-symlink-license by referring
    to /usr/share/common-licenses/GPL-2.
  * Enable all hardening flags.
    + Add patch to fix issues that have been found that way.
  * Add patch to fix spelling error found by Lintian.
  * Convert debian/copyright to machine-readable DEP-5 format.

 -- Axel Beckert <abe@debian.org>  Sat, 22 Jul 2017 18:15:19 +0200

xfrisk (1.2-3) unstable; urgency=low

  * Ack NMU, closes: #370232.
  * headers for libxaw are now in the libxaw7-dev package, closes: #484187.
  * Fix path to FAQ in doc-base entry, closes: #285242.
  * debian/menu: quote all fields, closes: #237640; update section.
  * Misc pkg cleanup:
    + bump debhelper compat to 6.
    + bump std. ver. to 3.8.0.

 -- Joe Nahmias <jello@debian.org>  Fri, 06 Jun 2008 01:40:23 -0400

xfrisk (1.2-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add libxaw-headers to the build dependencies (Closes: #370232)
  * Quote menu entries
  * Bump standards version

 -- Julien Danjou <acid@debian.org>  Sat, 10 Jun 2006 13:06:36 +0200

xfrisk (1.2-2) unstable; urgency=low

  * New maintainer (closes: #229262).
  * Acknowledge NMU (closes: #85309).
  * Fix client-only menu item to use localhost (closes: #215411).
  * Use xgethostname to remove hostname length limitations (closes: #107314).
  * Updated: debian/watch, debian/copyright, debian/control.
  * rewrote manpages.

 -- Joe Nahmias <jello@debian.org>  Mon, 16 Feb 2004 12:23:59 -0500

xfrisk (1.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Added Build-Depends, fixes Bug#85309.
  * Removed dh_suidregister from debian/rules (obsolete).
  * Removed .ex files.
  * Removed upstream's INSTALL documentation.

 -- Lenart Janos <ocsi@debian.org>  Sat, 24 Feb 2001 02:50:08 +0100

xfrisk (1.2-1) unstable; urgency=low

  * Initial Release.
  * First attempt at packaging xfrisk

 -- John O'Sullivan <jos@debian.org>  Thu, 15 Jun 2000 02:12:44 +0100
